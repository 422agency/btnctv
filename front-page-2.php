<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero', 'none' ); ?>
<?php endif; ?>

<div class="top-stories">
  <div class="container">
    <div class="row no-gutters">
      
      <?php
        $args = Array( 
          'post_type' => 'top_story',
		  'posts_per_page' => '3'
        );
        $the_query = new WP_Query( $args );
		$first=true;
      ?>

      <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
	  
        $thumbnail_id = get_post_thumbnail_id(); 
        $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id, 'thumbnail-size', true );
		
  	  	if($first){
  		  $first=false;

	     ?>

      <div class="top-story stories col-md-8" style="background-image: url(<?php echo $thumbnail_url[0]; ?>);">
        <div class="story-wrap">
          <div class="top-story-details col-md-10">
              <p><?php the_author(); ?></p>
              <h1><?php the_title(); ?></h1>
 				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><span>Read More</span></a>
            </div>
        </div>
      </div>

      <div class="col-md-4">
      <?php }else{ ?>
	  	 <div class="top-story stories col-md-12" style="background-image: url(<?php echo $thumbnail_url[0]; ?>);">
          <div class="story-wrap">
            <div class="top-story-details">
                <h6><?php the_author(); ?></h6>
                <h3><?php the_title(); ?> </h3>
                <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><span>Read More</span></a>
              </div>
          </div>
        </div>
	  <?php } endwhile; endif; ?>
         
      </div>
      
    </div>
  </div>
</div>

<section class="mission-statment" style="background-image: url(http://btnctv.422agency.com/wp-content/uploads/2017/02/news-room.jpg);">
	<div class="container">
		<div class="row no-gutters">
			<div class="col-md-10 offset-md-1">
				<h2>We are BTNC.</h2>
				<p>The mission of BTNC is to produce intelligent programming that is informative, educational, entertaining, inspiring, and empowering for distribution to the network’s African American audience. </p>
			</div>
		</div>
	</div>
</section>

<section class="the-culture">	
	<div class="container-fluid">
		<div class="row no-gutters">
			<div class="col-md-4 culture-content" style="background-image: url(http://btnctv.422agency.com/wp-content/uploads/2017/02/IMG_2244-3-compressor.jpg);">
				<?php if(dynamic_sidebar('front-left')); ?>
			</div>
			<div class="col-md-4 culture-content" style="background-image: url(http://btnctv.422agency.com/wp-content/uploads/2017/02/IMG_2244-compressor.jpg);">
				<?php if(dynamic_sidebar('front-center')); ?>
			</div>
			<div class="col-md-4 culture-content" style="background-image: url(http://btnctv.422agency.com/wp-content/uploads/2017/02/IMG_2244-2-compressor.jpg);">
				<?php if(dynamic_sidebar('front-right')); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
