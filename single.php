<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();?>

</div>

<div class="main-page-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>

			<hr>

			<?php comments_template(); ?>

			<?php endwhile; else: ?>

			<div class="page-header">
			<h1>Oh no!</h1>
			</div>

			<p>No content is appearing for this page!</p>

			<?php endif; ?>

			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
