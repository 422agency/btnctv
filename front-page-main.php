<?php
/*
  Template Name: Main Template
*/

?>
<?php get_header(); ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php if ( has_post_thumbnail() ) {
	
	// Get the post thumbnail URL
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
} else {
	
	// Get the default featured image in theme options
	$feat_image = get_field('default_featured_image', 'option');
} ?>
   
  <div class="default-page-header" style="background-image: url(<?php echo $feat_image; ?>); padding: 130px 0">
   <div class="container">
     <div class="row">
       <div class="col-md-8 offset-md-2">
        <h1><?php the_title(); ?></h1>
        <h4>Some Sub title goes here.</h4>
       </div>
     </div>
   </div>
  </div>
     
<!--     <div class="container">   
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <?php //the_content(); ?>
        </div>      
      </div>
    </div> -->

      <?php endwhile; else: ?>
      <div class="container">   
        <div class="row">
          <div class="col-md-12">

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>
      </div>      

    </div>
</div>
<?php endif; ?>

<section class="mission-statment" style="background-image: url(http://btnctv.422agency.com/wp-content/uploads/2017/02/news-room.jpg);">
  <div class="container">
    <div class="row no-gutters">
      <div class="col-md-10 offset-md-1">
        <h2>We are BTNC.</h2>
        <p>The mission of BTNC is to produce intelligent programming that is informative, educational, entertaining, inspiring, and empowering for distribution to the network’s African American audience. </p>
      </div>
    </div>
  </div>
</section>

<section class="the-culture"> 
  <div class="container-fluid">
    <div class="row no-gutters">
      <div class="col-md-4 culture-content" style="background-image: url(http://btnctv.422agency.com/wp-content/uploads/2017/02/IMG_2244-3-compressor.jpg);">
        <?php if(dynamic_sidebar('front-left')); ?>
      </div>
      <div class="col-md-4 culture-content" style="background-image: url(http://btnctv.422agency.com/wp-content/uploads/2017/02/IMG_2244-compressor.jpg);">
        <?php if(dynamic_sidebar('front-center')); ?>
      </div>
      <div class="col-md-4 culture-content" style="background-image: url(http://btnctv.422agency.com/wp-content/uploads/2017/02/IMG_2244-2-compressor.jpg);">
        <?php if(dynamic_sidebar('front-right')); ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>