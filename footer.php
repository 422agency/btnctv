<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

?>

<footer>

	<div class="wrapper" id="wrapper-footer">

		<div class="container">

			<div class="row no-gutters">
				<div class="col-md-8">
					<footer class="site-footer" id="colophon">
						<h4>Stay Tuned</h4>
						<h5>BTNC has set its launch date for Black History Month of 2018! Stay tuned for network updates and behind the scenes previews, as we chronicle our pre-launch activities.</h5>

					<div class="copyright">
						<div class="row no-gutters">
							<p>&copy; 2017 BTNC, All Rights Reserved.</p>
						</div>
					</div>

					</footer><!-- #colophon -->

				</div>

				<div class="col-md-4">
					<div class="row no-gutters">
						<div class="col-md-6">
	                         <?php wp_nav_menu(
                                array(
                                    'theme_location' => 'footer-menu-1',
                                    'container_class' => 'footer-contain',
                                    'menu_class' => 'footer-link',
                                    'fallback_cb' => '',
                                    'menu_id' => 'footer-menu1',
                                    'walker' => new wp_bootstrap_navwalker()
                                )
	                            ); ?>
						</div>
						<div class="col-md-6">
	                         <?php wp_nav_menu(
                                array(
                                    'theme_location' => 'footer-menu-2',
                                    'container_class' => 'footer-contain',
                                    'menu_class' => 'footer-link',
                                    'fallback_cb' => '',
                                    'menu_id' => 'footer-menu2',
                                    'walker' => new wp_bootstrap_navwalker()
                                )
	                            ); ?>
						</div>
					</div>
				</div>

			</div><!-- row end -->

		</div><!-- container end -->

	</div><!-- wrapper end -->

	</div><!-- #page -->
</footer>

<?php wp_footer(); ?>
<script src="//harshen.github.io/jquery-countdownTimer/jquery.countdownTimer.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/ytplayer/js/jquery.mb.YTPlayer.min.js"></script>
<script>
jQuery(function(){
        var stickyHeaderTop = 150;
		var setVal=true;
        jQuery(window).scroll(function(){
			if(jQuery(window).width() > 543){
                if( jQuery(window).scrollTop() > stickyHeaderTop && setVal) {
                        jQuery('#navbar-menu').addClass('fixed-menu');
						jQuery('#navbar-menu').css("position","absolute");
						jQuery('#navbar-menu').css("display","none");
						  jQuery('#navbar-menu').fadeIn( "slow", function() {
 							 });
						/*jQuery('#navbar-menu').find('.navbar-brand').animate( {padding:'0px'}, {duration:500});
						jQuery('#navbar-menu').find('.navbar-brand img').animate( {margin:'-3px',top:'0%'}, {duration:500});
						jQuery('#navbar-menu').find('.navbar-nav .nav-link').animate( {'padding-top':'1.2rem'}, {duration:500});*/
						setVal=false;
                } else if(jQuery(window).scrollTop() < stickyHeaderTop && !setVal) {
                        jQuery('#navbar-menu').removeClass('fixed-menu');
						jQuery('#navbar-menu').css("position","absolute");
					/*	jQuery('#navbar-menu').find('.navbar-brand').animate( {padding:'4px'}, {duration:500});
						jQuery('#navbar-menu').find('.navbar-brand img').animate( {margin:'0',top:'-30%'}, {duration:500});
						jQuery('#navbar-menu').find('.navbar-nav .nav-link').animate( {'padding-top':'1.75rem'}, {duration:500});*/
						setVal=true;
                }
				}
        });
  })
</script>

<script>
	jQuery(function() {
		jQuery('.future_date').countdowntimer({
		      dateAndTime : "2018/03/22 12:50:00",
		});

		// Disabled on mobile devices, because the video background doesn't work on mobile devices (instead, the background image is displayed).
		if (!jQuery.browser.mobile){ 
			jQuery(".youtube-bg").mb_YTPlayer();
			jQuery(".youtube-bg").css("padding","350px");
		}
	});
</script>

</body>
</html>
