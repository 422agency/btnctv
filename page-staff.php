<?php
/*
  Template Name: Staff Template
*/

?>
<?php get_header(); ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php if ( has_post_thumbnail() ) {
	
	// Get the post thumbnail URL
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
} else {
	
	// Get the default featured image in theme options
	$feat_image = get_field('default_featured_image', 'option');
} ?>
   
  <div class="default-page-header" style="background-image: url(<?php echo $feat_image; ?>); padding: 130px 0">
   <div class="container">
     <div class="row">
       <div class="col-md-8 offset-md-2">
        <h1><?php the_title(); ?></h1>
        <p><?php echo get_field( "sub_title" ); ?></p>
       </div>
     </div>
   </div>
  </div>
     
    <div class="container">   
      <div class="row"  style="padding-bottom: 20px;">
      <?php $args = array( 'post_type' => 'bio', 'posts_per_page' => -1 );
			$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post();
		  $feat_image1 = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
 		 ?>
                 <div class="col-md-4"  style="padding: 2px;">
         <img src="<?php echo $feat_image1; ?>"  />
        </div> 
         <?php
		endwhile; ?>   
      </div>
    </div>

      <?php endwhile; else: ?>
      <div class="container">   
        <div class="row">
          <div class="col-md-12">

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>
      </div>      

    </div>
</div>
        <?php endif; ?>



<?php get_footer(); ?>