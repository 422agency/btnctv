<?php
/*
  Template Name: Hero Image Template
*/

?>
<?php get_header(); ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php if ( has_post_thumbnail() ) {
	
	// Get the post thumbnail URL
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
} else {
	
	// Get the default featured image in theme options
	$feat_image = get_field('default_featured_image', 'option');
} ?>
   
  <div class="home-page-header" style="background-image: url(<?php echo $feat_image; ?>);">
   <div class="container">
     <div class="row">
       <div class="col-md-8 offset-md-2 text-center">
        <h1>welcome to BTNC</h1>
        <h6>Inform. Educate. Entertain. Inspire.</h6>
        <!-- <p>Inform. Educate. Entertain. Inspire.</p> -->
       </div>
     </div>
   </div>
  </div>
     
<!--     <div class="container">   
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <?php //the_content(); ?>
        </div>      
      </div>
    </div> -->

      <?php endwhile; else: ?>
      <div class="container">   
        <div class="row">
          <div class="col-md-12">

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>
      </div>      

    </div>
</div>
<?php endif; ?>

<section class="mission-statment">
  <div class="container">
    <div class="row no-gutters">
      <div class="col-md-10 offset-md-1">
        <h2>We are BTNC.</h2>
        <p>The mission of BTNC is to produce intelligent programming that is informative, educational, entertaining, inspiring, and empowering for distribution to the network’s African American audience. </p>
      </div>
    </div>
  </div>
</section>


<div class="collage">
  <div class="feature-content">
    <div class="container">
      <div class="row no-gutters">
      
        <div class="col-md-8">
          <div class="col-md-12 service-item">
            <img src="//btnctv.422agency.com/wp-content/uploads/2017/02/impulse-1.jpg" alt="img" />
          </div>
       <div class="col-md-6 service-item">
          <img src="//btnctv.422agency.com/wp-content/uploads/2017/02/impulse-3.jpg" alt="" />
          </div>
        <div class="col-md-6 service-item">
          <img src="//btnctv.422agency.com/wp-content/uploads/2017/02/impulse-2.jpg" alt="" />
          </div>
        </div>
         
        <div class="col-md-4 text-center">
          <div class="col-sm-12 feature-service watts" style="background-image: url(//btnctv.422agency.com/wp-content/uploads/2017/02/watts.jpg);">
              <div class="about-content-wrpper">
               <!-- <h1>JC Watts</h1> -->
               <p>Meet the Executive Team behind the network</p>
               <hr>
               <a href="//btnctv.422agency.com/leadership/" class="btn btn-primary btn-lg" role="button" aria-pressed="true">Meet the Team</a>
              </div>
          </div>
        </div>

        </div>
      </div>
    </div>

  <div class="feature-content">
    <div class="container">
      <div class="row no-gutters">

          <div class="col-md-4 service-item">
            <img src="//btnctv.422agency.com/wp-content/uploads/2017/02/impulse-4.jpg" alt="img" />
          </div> 

          <div class="col-md-8 service-item">
            <img src="//btnctv.422agency.com/wp-content/uploads/2017/02/impulse-5.jpg" alt="img" />
          </div> 

      </div>
    </div>
  </div>
</div>


</div>

<?php get_footer(); ?>