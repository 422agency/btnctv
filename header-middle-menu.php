<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> | <?php bloginfo('description'); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>

<!-- TypeKit fonts -->
<script src="https://use.typekit.net/mux0pdk.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">
    
    <!-- ******************* The Navbar Area ******************* -->
    <div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">
    
        <a class="skip-link screen-reader-text sr-only" href="#content"><?php _e( 'Skip to content', 'understrap' ); ?></a>

        <section class="countdown">
            <div class="container">
                <div class="offset-md-2 col-md-8 counter" style="background-image: url(//btnctv.422agency.com/wp-content/uploads/2017/02/clapper.jpeg);">
                        <div id="countdowntimer">
                            <h3><span class="future_date"></span></h3>
                            <p>Days : Hours : Minutes : Seconds</p>
                            <h6>Until the official launch of BTNC...</time></h6>
                        </div>
                </div>
            </div>
        </section>

        <nav id="navbar-menu" class="navbar navbar-dark site-navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">    
                <div class="container"><div class="center-it">
                            <div class="navbar-header">

                                <!-- .navbar-toggle is used as the toggle for collapsed navbar content -->

                                  <button class="navbar-toggle hidden-sm-up" type="button" data-toggle="collapse" data-target=".exCollapsingNavbar" onclick="jQuery('.exCollapsingNavbar1').toggleClass('in show');">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                            </div>
                             <a class="navbar-brand hidden-sm-up" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" style="float:left;"><img id="logo-navbar-middle" src="<?php echo get_template_directory_uri(); ?>/logo.png" width="120" style="position: relative; margin: 0px; top: -20%; z-index: 999; left:60px;" ></a>
                            <!-- The WordPress Menu goes here -->
                            <?php wp_nav_menu(
                                    array(
                                        'theme_location' => 'primary-right',
                                        'container_class' => 'collapse navbar-toggleable-xs exCollapsingNavbar',
                                        'menu_class' => 'nav navbar-nav',
                                        'fallback_cb' => '',
                                        'menu_id' => 'main-menu1',
                                        'walker' => new wp_bootstrap_navwalker()
                                    )
                            ); ?>
                            <!-- Your site title as branding in the menu -->
                            <a class="navbar-brand hidden-xs-down" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img id="logo-navbar-middle" src="<?php echo get_template_directory_uri(); ?>/logo.png" width="120" style=" position: relative; top: -20%;" ></a>
 
                            <!-- The WordPress Menu goes here -->
                            <?php wp_nav_menu(
                                    array(
                                        'theme_location' => 'primary-left',
                                        'container_class' => 'collapse navbar-toggleable-xs exCollapsingNavbar1',
                                        'menu_class' => 'nav navbar-nav',
                                        'fallback_cb' => '',
                                        'menu_id' => 'main-menu2',
                                        'walker' => new wp_bootstrap_navwalker()
                                    )
                            ); ?>
        </div>
                </div> <!-- .container -->
        </nav><!-- .site-navigation -->
    </div><!-- .wrapper-navbar end -->